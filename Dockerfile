FROM ubuntu:16.04

WORKDIR /app

RUN apt update
RUN apt install -y curl
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt install -y ffmpeg nodejs


COPY script.js .

CMD nodejs /app/script.js
