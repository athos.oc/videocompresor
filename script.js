const fs = require('fs');
const { exec } = require('child_process');


var datos = fs.readdirSync('/app/videos')
// console.log(datos);

async function main(){
  for(var i=0; i<datos.length;i++){
    if(datos[i]=='.DS_Store') continue
    console.log(datos[i]);
    await compress(datos[i])
  }
}
main()


function compress(videoName){
  return new Promise((resolve,reject)=>{
    const command = `ffmpeg -i /app/videos/${videoName} -strict -2 -vcodec libx264 -crf $RATIO /app/videos/out.${videoName}`
    exec(command, (err, stdout, stderr) => {
      if (err) {
        // node couldn't execute the command
        reject()
      }

      // the *entire* stdout and stderr (buffered)
      console.log(`stdout: ${stdout}`);
      console.log(`stderr: ${stderr}`);
      resolve()
    });

  })
}
